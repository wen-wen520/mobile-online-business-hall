<%@ page import="JDBC.login" %>
<%@ page import="JDBC.Bill" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Date" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="JDBC.entity_bill" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.google.gson.Gson"%>
<%@ page isELIgnored="false"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>移动网上营业厅账单查询</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body {
            font-family: "Microsoft YaHei", sans-serif;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        .form {
            margin-bottom: 20px;
        }

        input {
            width: 150px;
            padding: 5px;
        }

        select {
            padding: 5px;
        }

        button {
            padding: 5px 10px;
            border-radius: 5px;
            background-color: #4CAF50;
            color: white;
            cursor: pointer;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }

        th, td {
            text-align: center;
            padding: 10px;
            border: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }

        .total {
            text-align: right;
            font-size: 18px;
            font-weight: bold;
        }

    </style>
    <script type="text/javascript" src="jquery.min.js"></script>

</head>
<body>
<div class="container">
    <h1>移动网上营业厅账单查询</h1>
    <div class="form">
        <label for="customer-name">客户姓名：</label>
        <input type="text" id="customer-name" value="待查询" disabled><!-- 此处设置输入框为禁用，防止用户修改客户姓名 -->
        <br>
        <form id="check">
            <label for="month">选择月份：</label>
            <select id="month">
                <option value="2023-05" selected>2023-05</option>
                <option value="2023-04">2023-04</option>
                <option value="2023-04">2023-03</option>
                <option value="2023-02">2023-02</option>
                <option value="2023-01">2023-01</option>
                <option value="2022-12">2022-12</option>
                <option value="2022-11">2022-11</option>
                <option value="2022-10">2022-10</option>
                <option value="2022-09">2022-09</option>
                <option value="2022-08">2022-08</option>
                <option value="2022-07">2022-07</option>
                <option value="2022-06">2022-06</option>
            </select>
        </form>
        <br>
        <button onclick="query()">查询</button>
    </div>
    <table id="bill-table">
        <thead>
        <tr>
            <th>消费项目</th>
            <th>消费金额</th>
        </tr>
        </thead>
        <tbody></tbody>
    </table>
    <div class="total"></div>
</div>
<script>
    // 获取当前日期及时间
    const currentDate = new Date();

    // 获取选择月份的下拉框元素
    const monthSelect = document.querySelector("#month");


    // 定义查询函数
    function query() {
        // 获取用户选择的月份
        const selectedMonth = monthSelect.value;
        var inputElement = document.getElementById("customer-name");

        // 创建XHR对象
        var xhr = new XMLHttpRequest();
        var formData = new FormData(document.getElementById("check"));
// 指定请求方法、URL和是否异步
        xhr.open("get", "/untitled2_war_exploded/ServletBill?month="+selectedMonth.toString(), true);
// 设置请求头
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
// 将数据作为参数发送请求
        xhr.send();
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4 && xhr.status == 200) {
                var bills = JSON.parse(xhr.responseText);
                    alert("成功！");
                    // 清空表格内容
                    const tableBody = document.querySelector("#bill-table tbody");
                    tableBody.innerHTML = "";
                    let totalAmount = 0;
                    inputElement.value = bills[0]['Uname'];
                    for (var i = 0; i < bills.length; i++) {
                        var bill = bills[i];
                        const row = `
                            <tr>
                                <td>`+bill['ChItem']+`</td>
                                <td>￥`+bill['MoneyChange']+`</td>
                            </tr>
                        `;

                        tableBody.insertAdjacentHTML("beforeend", row); // 使用 insertAdjacentHTML 函数可以避免频繁操作 DOM，提高性能
                        totalAmount+=parseFloat(bill['MoneyChange']);
                    }

                    // 更新总消费金额
                    const totalDiv = document.querySelector(".total");
                    totalDiv.innerText = `您当月共消费￥`+totalAmount.toFixed(2);
                // 处理响应数据
            } else {
                console.log('请求失败！');
            }
        };

    }

</script>
</body>
</html>
