package JDBC;

import java.util.Date;

public class entity_bill {
    private String tel;
    private String Uname;
    private String ChItem;
    private Date ChTime;
    private float MoneyChange;

    public entity_bill(String tel, String uname, String chItem, Date chTime, float moneyChange) {
        this.tel = tel;
        Uname = uname;
        ChItem = chItem;
        ChTime = chTime;
        MoneyChange = moneyChange;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getChItem() {
        return ChItem;
    }

    public void setChItem(String chItem) {
        ChItem = chItem;
    }

    public Date getChTime() {
        return ChTime;
    }

    public void setChTime(Date chTime) {
        ChTime = chTime;
    }

    public float getMoneyChange() {
        return MoneyChange;
    }

    public void setMoneyChange(float moneyChange) {
        MoneyChange = moneyChange;
    }
}
