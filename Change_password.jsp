<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item">主页</a>
    <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
    <a href="self_message.jsp" class="menu-item">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item active">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <title>修改密码</title>
    <style>
        body {
            font-family: sans-serif;
            background-color:#ffffff;
        }

        h1 {
            text-align: center;
        }

        form {
            width: 400px;
            margin: 0 auto;
            background-color: #ccc;
            padding: 20px;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(142, 138, 138, 0.3);
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type=password] {
            display: block;
            width: 100%;
            padding: 10px;
            border-radius: 3px;
            border: 1px solid #ccc;
            margin-bottom: 20px;
        }

        button[type=submit] {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: #141dc2;
            color: #fff;
            border: 0;
            border-radius: 3px;
            cursor: pointer;
        }
    </style>

    <h1>修改密码</h1>
    <form action="Change.jsp">
        <label for="current_password">当前密码:</label>
        <input type="password" id="current_password" name="current_password">

        <label for="new_password">新密码:</label>
        <input type="password" id="new_password" name="new_password">

        <label for="confirm_password">确认密码:</label>
        <input type="password" id="confirm_password" name="confirm_password">

        <button type="submit">确认</button>
    </form>
</div>
</body>
</html>
