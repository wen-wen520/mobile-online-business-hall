package JDBC;

import com.google.gson.Gson;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ServletBill", value = "/ServletBill")
public class ServletBill extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("tex/html;charset=utf-8");
//        String w = request.getParameter("month");
//        response.setCharacterEncoding("utf-8");
//        Gson gson = new Gson();
//
//        String result = gson.toJson(w);
//        response.getWriter().append(result);


        login user = new login();

        Bill select = new Bill();
        List<entity_bill> bills =new ArrayList<>();

        String number = user.getNumber();

        // 获取前端传来的月份参数
        String month = request.getParameter("month");
        System.out.println("查询的月份为：" + month);

        // 进行相应的处理操作
        ResultSet rs = select.bill(number,month);
        System.out.println(month);

        while(true){
            try {
                if (!rs.next()) break;
                else{
                    entity_bill b = new entity_bill(
                        rs.getString(1), // tel
                        rs.getString(2), // Uname
                        rs.getString(3), // ChItem
                        rs.getDate(4),   // ChTime
                        rs.getFloat(5)   // MoneyChange
                    );
                    bills.add(b); // 将新构建的实例添加到 bills 列表中
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        }
        System.out.println(bills.size());
        for (int i = 0; i < bills.size(); i++) {
            System.out.println(bills.get(i).getChItem());
        }
        response.setContentType("tex/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        Gson gson = new Gson();

        String result = gson.toJson(bills);
        response.getWriter().append(result);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
