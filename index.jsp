<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<html>

<head>
    <meta charset="UTF-8">
    <title>用户登录界面</title>
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            background-color: #f2f2f2;
            font-family: Arial, sans-serif;
        }

        .login-form {
            width: 40%;
            margin: 100px auto;
            padding: 40px;
            border-radius: 10px;
            background-color: #fff;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
        }

        h1 {
            text-align: center;
            color: #444;
        }

        input[type=text],
        input[type=password] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        button {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            width: 100%;
        }

        button:hover {
            background-color: #45a049;
        }

        .signup {
            text-align: center;
        }

        a {
            color: #4CAF50;
        }
    </style>
</head>

<body>

<form class="login-form" action="Verify_login.jsp" method="post">

    <h1>用户登录</h1>
    <label for="username">账号</label>
    <input type="text" id="username" name="username" required placeholder="请输入账号...">
    <label for="password">密码</label>
    <input type="password" id="password" name="password" required placeholder="请输入密码...">
    <button type="submit">登录</button>
    <div class="signup" >
        <p>还没有账号？<a href="AddPage.jsp">注册</a></p>
    </div>
</form>
</body>