<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item">主页</a>
    <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
    <a href="self_message.jsp" class="menu-item">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item active">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <p>这是一个动态面板</p>
    <p>左侧菜单栏可进行相应操作</p>
</div>
</body>
</html>
