<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item">主页</a>
    <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
    <a href="self_message.jsp" class="menu-item">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item active">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <title>维修单申报界面</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
        }

        form {
            background-color: #fff;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 2px 2px 5px #eee;
        }

        form p {
            margin-top: 0;
        }

        form input[type="text"],
        form textarea {
            display: block;
            padding: 10px;
            width: 100%;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 16px;
        }

        form input[type="submit"] {
            background-color: #4CAF50;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            font-size: 16px;
            cursor: pointer;
        }

        form input[type="submit"]:hover {
            background-color: #3e8e41;
        }

        /* 新加的样式 */
        h1 {
            text-align: center;
            font-size: 30px;
            margin-top: 50px;
        }

        input[type="date"] {
            display: block;
            padding: 10px;
            width: 100%;
            margin-bottom: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
            font-size: 16px;
        }
    </style>

    <form action="mainten.jsp">
        <h1>维修单申报</h1> <!-- 新加的标题 -->
        <p>
            <label for="name">申报人姓名：</label>
            <input type="text" id="name" name="applicantName" required>
        </p>
        <p>
            <label for="phone">联系电话：</label>
            <input type="text" id="phone" name="phoneNumber" required>
        </p>
        <p>
            <label for="date">维修日期：</label>
            <input type="date" id="date" name="repairDate" required>
        </p>
        <p>
            <label for="description">问题描述：</label>
            <textarea id="description" name="problemDescription" rows="10" required></textarea>
        </p>
        <p>
            <input type="submit" value="提交申请">
        </p>
    </form>
</div>
</body>
</html>
