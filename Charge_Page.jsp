<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item">主页</a>
    <a href="Charge_Page.jsp" class="menu-item active">充值缴费</a>
    <a href="self_message.jsp" class="menu-item">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <title>移动营业厅充值缴费</title>
    <style>
        .container {
            width: 500px;
            margin: auto;
            text-align: center;
        }
        h1 {
            font-size: 36px;
            margin-bottom: 30px;
        }
        form {
            display: inline-block;
            text-align: left;
            border: 1px solid #ccc;
            border-radius: 5px;
            padding: 50px;
            margin-bottom: 20px;
        }
        label {
            font-size: 18px;
            display: block;
            margin-bottom: 10px;
        }
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            box-sizing: border-box;
            border: none;
            border-radius: 4px;
            background-color: #f8f8f8;
        }
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>

    <div class="container">
        <h1>充值中心</h1>

        <div class="recharge">
            <h2>话费充值</h2>
            <form action="Charge_amount1.jsp">
                <!-- <label for="phone-number">手机号码：</label>
                <input type="tel" id="phone-number" name="phone-number" placeholder="请输入手机号码"><br><br> -->

                <label for="amount1">充值金额：</label>
                <select id="amount1" name="amount1">
                    <option value="0">0元</option>
                    <option value="10">10元</option>
                    <option value="20">20元</option>
                    <option value="50">50元</option>
                    <option value="100">100元</option>
                </select><br><br>

                <input type="submit" value="确认充值">
            </form>
        </div>

        <div class="recharge">
            <h2>流量充值</h2>
            <form action="Charge_amount2.jsp" method="">
                <!-- <label for="phone-number">手机号码：</label>
                <input type="tel" id="phone-number" name="phone-number" placeholder="请输入手机号码"><br><br> -->

                <label for="amount2">充值金额：</label>
                <select id="amount2" name="amount2">
                    <option value="0">0GB</option>
                    <option value="1">1GB</option>
                    <option value="2">2GB</option>
                    <option value="5">5GB</option>
                    <option value="10">10GB</option>
                </select><br><br>

                <input type="submit" value="确认充值">
            </form>
        </div>
</div>

</body>
</html>
