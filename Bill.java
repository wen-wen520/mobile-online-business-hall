package JDBC;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Bill {
    private  String number;

    public ResultSet bill(String number, String mon) {
        String url = "jdbc:mysql://localhost:3306/test";
        String username = "root";
        String password = "123456";

        try {
            String dateString = mon;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
            Date date = sdf.parse(dateString);
            System.out.println(date);

            // 加载 JDBC 驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");

            // 建立数据库连接
            Connection conn = DriverManager.getConnection(url, username, password);

            // 准备 SQL 查询语句
            String sql = "SELECT * FROM bill WHERE tel = ? AND YEAR(ChTime) = YEAR(?) AND MONTH(ChTime) = MONTH(?)";
//            PreparedStatement pstmt = conn.prepareStatement(sql);
            PreparedStatement pstmt = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
//            pstmt.executeQuery(sql);
            pstmt.setString(1, number);
            pstmt.setDate(2, new java.sql.Date(date.getTime()));
            pstmt.setDate(3, new java.sql.Date(date.getTime()));

            // 执行查询并处理结果
            ResultSet rs = pstmt.executeQuery();
            if (!rs.next()) {
                System.out.println("查询结果集为空");

            } else {
                System.out.println("查询结果集不为空");
//                rs.beforeFirst();
            }
            rs.beforeFirst();
            return rs;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}

