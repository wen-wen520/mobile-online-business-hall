package JDBC;

import java.sql.*;

public class Add {
    public static void add(String name,String sex,int age,String number,String email,String password1,String self){
        //数据库路径，其中yiibaidb是我的数据库名称
        String url = "jdbc:mysql://localhost:3306/big";
        //告诉连接你的账号密码
        String username = "root";
        String password = "zmh030716";

        try {

            //反射加载jdbc的Driver类
            Class.forName("com.mysql.cj.jdbc.Driver");
            //通过DriverManager获取Connection对象
            Connection con = DriverManager.getConnection(url,username,password);

            String sql = "insert into cust(name,sex,age,number,email,password,self) values ('"+name+"','"+sex+"',"+age+",'"+number+"','"+email+"','"+password1+"','"+self+"')";
            //通过PrepardStatement 准备sql语句
            PreparedStatement statement = con.prepareStatement(sql);
            Statement s = con.createStatement();
            int result = s.executeUpdate(sql);
            if(result==1)
            {
                System.out.println("新增成功");
            }else {
                System.out.println("新增失败");
            }
            //获取结果集
            ResultSet rs = statement.executeQuery();

            //声明结果集的结构
/*        ResultSetMetaData rsmd = rs.getMetaData();

        //获取结果集的列数
        int count = rsmd.getColumnCount();

        //对结果集进行遍历
        while(rs.next())
        {

            out.println("");
            for(int i=1;i<count;i++)
            {
                out.println(rs.getString(i));
            }

            out.println("<br>");
        }*/
            //关闭我们的连接
            rs.close();

            statement.close();

            con.close();

        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
}
