<%@ page import="JDBC.login" %>
<%@ page import="JDBC.select_selfmessage" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>个人信息</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item">主页</a>
    <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
    <a href="self_message.jsp" class="menu-item active">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <title>个人信息</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f5f5f5;
        }

        .container {
            max-width: 800px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.1);
            border-radius: 5px;
        }

        h1 {
            margin-top: 0px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:hover {
            background-color: #f5f5f5;
        }

        td:first-child {
            font-weight: bold;
        }

    </style>
    <%
        login login = new login();
        String number = login.getNumber();
        String self = null,meal = null;
        int balance = 0,voide = 0,integral = 0;
        select_selfmessage select = new select_selfmessage();
        ResultSet rs = select.select(number);
        if(rs.next())
        {
           self = rs.getString(2);
           meal = rs.getString(3);
           balance = rs.getInt(4);
           voide = rs.getInt(5);
           integral = rs.getInt(6);
        }
    %>
    <div class="container">
        <h1>个人信息</h1>
        <table>
            <tr>
                <th>类型</th>
                <th>信息</th>
            </tr>
            <tr>
                <td>电话号码</td>
                <td><%=number%></td>
            </tr>
            <tr>
                <td>身份证号码</td>
                <td><%=self%></td>
            </tr>
            <tr>
                <td>开户信息</td>
                <td>中国移动</td>
            </tr>
            <tr>
                <td>订购套餐</td>
                <td><%=meal%></td>
            </tr>
            <tr>
                <td>话费余额</td>
                <td><%=balance%>元</td>
            </tr>
            <tr>
                <td>流量剩余</td>
                <td><%=voide%>G</td>
            </tr>
            <tr>
                <td>积分余额</td>
                <td><%=integral%>分</td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
