<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <meta charset="UTF-8">
  <title>客服服务</title>
  <style>
    /* 样式表 */
    body {
      font-family: Arial, sans-serif;
      margin: 0;
      padding: 0;
    }

    #menu {
      float: left;
      width: 200px;
      height: 100vh;
      background-color: #f2f2f2;
    }

    #panel {
      margin-left: 200px;
      padding: 20px;
    }

    /* 菜单样式 */
    .menu-item {
      display: block;
      padding: 10px;
      text-align: center;
      color: #666;
      border-bottom: 1px solid #ccc;
      text-decoration: none;
    }

    .menu-item:hover{
      cursor: pointer;
      background-color: #efefef;
    }

    .menu-item.active {
      color: #000;
      background-color: #d9d9d9;
    }
  </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
  <a href="login.jsp" class="menu-item">主页</a>
  <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
  <a href="self_message.jsp" class="menu-item">个人信息</a>
  <a href="Bill_message.jsp" class="menu-item">账单信息</a>
  <a href="Customer_Service.jsp" class="menu-item active">客服服务</a>
  <a href="Change_password.jsp" class="menu-item">修改密码</a>
  <a href="Maintenance.jsp" class="menu-item">维修申请</a>
  <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
  <div class="chat-box">
    <div class="chat-header">
      <h3>与客服聊天</h3>
    </div>
    <div class="chat-body">
      <div class="chat-message">
        <p>你好，有什么可以帮助你吗？</p>
      </div>
      <div class="chat-message">
        <p>能告诉我你的问题吗？</p>
      </div>
    </div>
    <div class="chat-footer">
      <input type="text" placeholder="Type your message here...">
      <button>Send</button>
    </div>
  </div>
  <style>
    .chat-box {
      border: 1px solid #ccc;
      border-radius: 5px;
      width: 400px;
      height: 500px;
      margin: 20px auto;
      font-family: Arial, sans-serif;
    }

    .chat-header {
      background-color: #f2f2f2;
      padding: 10px;
      border-bottom: 1px solid #ccc;
    }

    .chat-header h3 {
      margin: 0;
      font-size: 18px;
      font-weight: bold;
    }

    .chat-body {
      padding: 10px;
      height: 400px;
      overflow-y: scroll;
    }

    .chat-message {
      margin-bottom: 10px;
    }

    .chat-message p {
      background-color: #f2f2f2;
      padding: 10px;
      border-radius: 5px;
      display: inline-block;
      max-width: 80%;
    }

    .chat-message p:last-child {
      margin-left: 20%;
    }

    .chat-footer {
      padding: 10px;
      border-top: 1px solid #ccc;
    }

    .chat-footer input[type="text"] {
      padding: 5px;
      border-radius: 5px;
      border: none;
      width: 80%;
    }

    .chat-footer button {
      padding: 5px;
      border-radius: 5px;
      border: none;
      background-color: #4CAF50;
      color: white;
      width: 18%;
      cursor: pointer;
    }

    .chat-footer button:hover {
      background-color: #3e8e41;
    }
  </style>

  <script>
    // 获取聊天框元素
    var chatBox = document.querySelector('.chat-box');

    // 获取输入框和发送按钮元素
    var inputBox = chatBox.querySelector('input[type="text"]');
    var sendButton = chatBox.querySelector('button');

    // 监听发送按钮的点击事件
    sendButton.addEventListener('click', function() {
      // 获取输入框的值
      var message = inputBox.value;

      // 如果输入框不为空，则创建新的聊天消息元素并添加到聊天框中
      if (message.trim() !== '') {
        var chatMessage = document.createElement('div');
        chatMessage.classList.add('chat-message');
        chatMessage.innerHTML = '<p>' + message + '</p>';
        chatBox.querySelector('.chat-body').appendChild(chatMessage);

        // 清空输入框
        inputBox.value = '';
      }
    });
  </script>

</div>
</body>
</html>
