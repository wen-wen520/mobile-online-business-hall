<%@ page import="JDBC.login" %>
<%@ page import="JDBC.select_selfmessage" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>主页</title>
    <style>
        /* 样式表 */
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
        }

        #menu {
            float: left;
            width: 200px;
            height: 100vh;
            background-color: #f2f2f2;
        }

        #panel {
            margin-left: 200px;
            padding: 20px;
        }

        /* 菜单样式 */
        .menu-item {
            display: block;
            padding: 10px;
            text-align: center;
            color: #666;
            border-bottom: 1px solid #ccc;
            text-decoration: none;
        }

        .menu-item:hover{
            cursor: pointer;
            background-color: #efefef;
        }

        .menu-item.active {
            color: #000;
            background-color: #d9d9d9;
        }
    </style>
</head>
<body>
<!-- 左侧菜单栏 -->
<div id="menu">
    <a href="login.jsp" class="menu-item active">主页</a>
    <a href="Charge_Page.jsp" class="menu-item">充值缴费</a>
    <a href="self_message.jsp" class="menu-item">个人信息</a>
    <a href="Bill_message.jsp" class="menu-item">账单信息</a>
    <a href="Customer_Service.jsp" class="menu-item">客服服务</a>
    <a href="Change_password.jsp" class="menu-item">修改密码</a>
    <a href="Maintenance.jsp" class="menu-item">维修申请</a>
    <a href="Mainten_histroy.jsp" class="menu-item">维修记录</a>
</div>

<!-- 右侧动态面板 -->
<div id="panel">
    <title>Title</title>
    <style>
        .container {
            width: 600px;
            margin: auto;
            padding: 20px;
            background-color: #ffffff;
        }

        .profile-pic {

            float: left;
            display: block;
            /*margin: 0 auto;*/
            border-radius: 50%;
            width: 200px;
            height: 200px;
            background-image: url("img/头像.png");
            background-size: cover;
        }

        .parent {
            text-align: right;
        }
        .large-img {
            width: 300px;
            height: 60px;
            float: right;
        }
        .small-img {
            width: 20px;
            height: 20px;
            float: right;
        }




        table {
            margin-top: 30px;
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #ffffff;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #ffffff;
            font-weight: bold;
            padding-top: 12px;
            padding-bottom: 12px;
        }

        tr:nth-child(even) {background-color: #f2f2f2;}

        <%--下方轮播图style--%>
        *{
            margin: 0;
            padding: 0;
        }
        ul{
            list-style: none;
        }
        #main{
            width: 952px;
            height: 450px;
            border: 1px solid #ccc;
            overflow: hidden;
            position: relative;
            margin: 20px auto;
        }

        #one{
            width: 150px;
            height: 30px;
            position: absolute;
            z-index: 100;
            right: 5px;
            bottom: 5px;
        }
        #one li{
            width: 15px;
            height: 15px;
            float: left;
            border-radius: 50%;
            border: 2px solid white;
            background: orange;
            margin-left: 5px;
        }

        #two{
            position: absolute;
        }
        #two li{
            width: 942px;
            height: 450px;
            border: none;
            display: none;
        }
    </style>

    <div class="container">
        <div class="profile-pic">
        </div>

        <div class="parent">
            <img src="img/中国移动.png" alt="Large Image" class="large-img">
            <a href="Change_password.jsp"><img src="img/设置.png" alt="Small Image" class="small-img"></a>
        </div>
        <%
            login login = new login();
            String number = login.getNumber();
            String self = null,meal = null;
            int balance = 0,voide = 0,integral = 0;
            select_selfmessage select = new select_selfmessage();
            ResultSet rs = select.select(number);
            if(rs.next())
            {
                balance = rs.getInt(4);
                voide = rs.getInt(5);
                integral = rs.getInt(6);
            }
        %>
        <table>
            <tr>
                <th>话费剩余</th>
                <td><%=balance%>元</td>
            </tr>
            <tr>
                <th>流量剩余</th>
                <td><%=voide%>GB</td>
            </tr>
            <tr>
                <th>积分剩余</th>
                <td><%=integral%>分</td>
            </tr>
        </table>
    </div>

    <div id="main">
        <%--下面的小圆点区域--%>
        <ul id="one">
            <li style="background: #F00;"></li>
            <li></li>
            <li></li>
        </ul>
        <%--轮播图区域--%>
        <ul id="two">
            <li style="display: block;">
                <img src="img/1.png">
            </li>
            <li>
                <img src="img/2.png">
            </li>
            <li>
                <img src="img/3.png">
            </li>

        </ul>
    </div>


    <script>
        //1.获取要操作的DOM节点对象
        var one = document.getElementById('one')
        var two = document.getElementById('two')

        var onelis = one.getElementsByTagName('li')
        var twolis = two.getElementsByTagName('li')

        var num = 0;

        var a = 0;

        var timer = setInterval(fun,2000)

        //遍历添加事件
        for(var i = 0; i < onelis.length; i++){

            //将每一个导航对象添加一个num属性
            onelis[i].num = i;

            onelis[i].onmouseover = function (){
                //清空定时器
                clearInterval(timer)

                num = this.num;
                //遍历所有的导航节点去除样式
                for(var j = 0; j < onelis.length; j++){
                    onelis[j].style.backgroundColor = 'orange';
                }
                //让当前鼠标所在的导航改变样式
                this.style.backgroundColor = '#F00';

                //遍历轮播图部分
                for(var k = 0; k < onelis.length; k++){

                    //判断
                    if(k==num){
                        twolis[num].style.display = 'block';
                    }else{
                        twolis[k].style.display = 'none';
                    }
                }
            }

            //鼠标移除导航事件
            onelis[i].onmouseout = function(){
                //让a和num的值先进行同步
                a = num;
                timer = null;
                timer = setInterval(fun,2000)
            }
        }

        function fun(){
            //将所有的导航初始化颜色
            for(var j = 0; j < onelis.length; j++){
                onelis[j].style.backgroundColor = 'orange';
            }

            //遍历轮播图节点
            for(var i = 0;i < twolis.length; i++){
                if(i == a){
                    twolis[a].style.display = 'block'
                }else{
                    twolis[i].style.display = 'none'
                }
            }
            onelis[a].style.backgroundColor = 'red';
            a++;
            if(a % onelis.length == 0){
                a = 0;
            }

        }
    </script>
</div>
</body>
</html>
